# Gematria Calculator

Simple Hebrew gematria calculator written in JavaScript.

This repo contains just the basic HTML and JS with no extraneous style code. The implementation on my website can be found here: [https://bosheshabayis.com/calculator/](https://bosheshabayis.com/calculator/).
